import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixExceptSelectPage } from './fix-except-select.page';

describe('FixExceptSelectPage', () => {
  let component: FixExceptSelectPage;
  let fixture: ComponentFixture<FixExceptSelectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixExceptSelectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixExceptSelectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
