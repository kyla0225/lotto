import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FixExceptSelectPage } from './fix-except-select.page';
import { TopHeaderComponent } from '../top-header/top-header.component';
import { BasicPopupSelectComponent } from '../basic-popup-select/basic-popup-select.component';

const routes: Routes = [
  {
    path: '',
    component: FixExceptSelectPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    FixExceptSelectPage,
    TopHeaderComponent,
    BasicPopupSelectComponent
  ]
})
export class FixExceptSelectPageModule {}
