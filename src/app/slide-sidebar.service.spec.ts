import { TestBed } from '@angular/core/testing';

import { SlideSidebarService } from './slide-sidebar.service';

describe('SlideSidebarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SlideSidebarService = TestBed.get(SlideSidebarService);
    expect(service).toBeTruthy();
  });
});
