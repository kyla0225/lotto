import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfTypedNumPage } from './self-typed-num.page';

describe('SelfTypedNumPage', () => {
  let component: SelfTypedNumPage;
  let fixture: ComponentFixture<SelfTypedNumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfTypedNumPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfTypedNumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
