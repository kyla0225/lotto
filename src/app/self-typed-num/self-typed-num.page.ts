import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-self-typed-num',
  templateUrl: './self-typed-num.page.html',
  styleUrls: ['./self-typed-num.page.scss'],
})
export class SelfTypedNumPage implements OnInit {

  lotto_arr = [];
  constructor() {
    for ( let j = 0; j < 45; j++ ) { this.lotto_arr[ j ] = ( j + 1 ) ; }
  }
  ngOnInit() {   }
}
