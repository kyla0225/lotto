import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SelfTypedNumPage } from './self-typed-num.page';
import { TopHeaderComponent } from '../top-header/top-header.component';

const routes: Routes = [
  {
    path: '',
    component: SelfTypedNumPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    SelfTypedNumPage,
    TopHeaderComponent
  ]
})
export class SelfTypedNumPageModule {}
