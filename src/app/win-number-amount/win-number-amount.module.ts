import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WinNumberAmountPage } from './win-number-amount.page';
import { TopHeaderComponent } from '../top-header/top-header.component';

const routes: Routes = [
  {
    path: '',
    component: WinNumberAmountPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    WinNumberAmountPage,
TopHeaderComponent
  ]
})
export class WinNumberAmountPageModule {}
