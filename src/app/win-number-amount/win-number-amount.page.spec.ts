import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinNumberAmountPage } from './win-number-amount.page';

describe('WinNumberAmountPage', () => {
  let component: WinNumberAmountPage;
  let fixture: ComponentFixture<WinNumberAmountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinNumberAmountPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinNumberAmountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
