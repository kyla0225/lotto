import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckWinAndPage } from './check-win-and.page';
import { TopHeaderComponent } from '../top-header/top-header.component';
import { BasicPopupComponent } from '../basic-popup/basic-popup.component';

const routes: Routes = [
  {
    path: '',
    component: CheckWinAndPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CheckWinAndPage,
    TopHeaderComponent,
    BasicPopupComponent
  ]
})
export class CheckWinAndPageModule {}
