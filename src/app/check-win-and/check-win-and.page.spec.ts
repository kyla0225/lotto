import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckWinAndPage } from './check-win-and.page';

describe('CheckWinAndPage', () => {
  let component: CheckWinAndPage;
  let fixture: ComponentFixture<CheckWinAndPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckWinAndPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckWinAndPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
