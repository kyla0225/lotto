import { Component, OnInit } from '@angular/core';
import { SlideSidebarService } from '../slide-sidebar.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent implements OnInit {
  constructor( public slide_service: SlideSidebarService ) { }

  ngOnInit() {
    this.slide_service.notice_change.subscribe( () => this.hamburger_click() );
  }

  /* 사이드바 메인 메뉴 토글 */
  toggle_menu(event) {
    const lis = document.querySelectorAll('.main_menu_item');
    const lis_leng = lis.length;
    const tg = event.target.parentNode;
    const is_on = tg.classList.contains('on');
    if ( !is_on ) {
      for ( let i = 0; i < lis_leng; i++ ) { lis[i].classList.remove('on'); }
      tg.classList.add('on');
    } else if ( is_on ) { tg.classList.remove('on'); }
  }

  hamburger_click() {
    return this.slide_service.hamburger_click();
  }

}
