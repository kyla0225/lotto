import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExceptFixSelectPage } from './except-fix-select.page';
import { TopHeaderComponent } from '../top-header/top-header.component';

const routes: Routes = [
  {
    path: '',
    component: ExceptFixSelectPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ExceptFixSelectPage,
    TopHeaderComponent
  ]
})
export class ExceptFixSelectPageModule {}
