import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceptFixSelectPage } from './except-fix-select.page';

describe('ExceptFixSelectPage', () => {
  let component: ExceptFixSelectPage;
  let fixture: ComponentFixture<ExceptFixSelectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceptFixSelectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceptFixSelectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
