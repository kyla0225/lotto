import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SlideSidebarService {
  toggled = false;
  notice_change: EventEmitter<any> = new EventEmitter<any>();
  constructor() {  }

  get_ham_bool() {
    return this.toggled;
  }

  set_ham_bool(state: boolean) {
    this.toggled = state;
  }
  hamburger_click() {
    const wrap = document.querySelector('.side_bar_contents');
    const overlay = document.querySelector('.overlay');
    
    if ( this.get_ham_bool() ) {
      wrap.classList.remove('sidebar_on');
      overlay.classList.remove('on');
    } else if ( !this.get_ham_bool() ) {
      wrap.classList.add('sidebar_on');
      overlay.classList.add('on');
    }
    this.set_ham_bool( !this.get_ham_bool() );
  }

}
