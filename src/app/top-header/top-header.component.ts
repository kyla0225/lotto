import { Component, OnInit } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { SlideSidebarService } from '../slide-sidebar.service';

@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.scss'],
})
export class TopHeaderComponent implements OnInit {

  top_header_tit = '로또1등이당';
  this_location = document.location.href;
  splash: any;
  here_header: string;
  header_obj = {
    'home' : '로또1등이당',
    'check-win-and' : '당첨확인&구입번호목록',
    'self-typed-num' : '구입번호 직접 입력',
    'saved-number' : '생성번호 저장목록',
    'create-random' : '랜덤생성',
    'create-self' : '직접생성',
    'win-number-amount' : '당첨번호&당첨금',
    'fix-except-select' : '고정수 지정',
    'except-fix-select' : '제외수 지정'
  };

  constructor( public slide_service: SlideSidebarService ) {

    this.splash = this.this_location.split('/');
    this.here_header =  this.header_obj[ this.splash[this.splash.length - 1] ];
    if ( this.here_header === undefined ) { this.top_header_tit = '헤더타이틀 미정'; return; }
    this.top_header_tit = this.here_header;
  }

  ngOnInit() {
    this.slide_service.notice_change.subscribe( () => this.hamburger_click() );
   }

  isHamburger() { if ( this.splash[this.splash.length - 1] === 'home' ) { return true; } }

  hamburger_click() {
    return this.slide_service.hamburger_click();
  }
}
