import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedNumberPage } from './saved-number.page';

describe('SavedNumberPage', () => {
  let component: SavedNumberPage;
  let fixture: ComponentFixture<SavedNumberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedNumberPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedNumberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
