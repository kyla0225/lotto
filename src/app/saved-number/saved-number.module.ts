import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SavedNumberPage } from './saved-number.page';
import { TopHeaderComponent } from '../top-header/top-header.component';

const routes: Routes = [
  {
    path: '',
    component: SavedNumberPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    SavedNumberPage,
    TopHeaderComponent
  ]
})
export class SavedNumberPageModule {}
