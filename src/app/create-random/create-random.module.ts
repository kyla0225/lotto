import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreateRandomPage } from './create-random.page';
import { TopHeaderComponent } from '../top-header/top-header.component';
import { BasicPopupHorizonComponent } from '../basic-popup-horizon/basic-popup-horizon.component';

const routes: Routes = [
  {
    path: '',
    component: CreateRandomPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CreateRandomPage,
    TopHeaderComponent,
    BasicPopupHorizonComponent
  ]
})
export class CreateRandomPageModule {}
