import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSelfPage } from './create-self.page';

describe('CreateSelfPage', () => {
  let component: CreateSelfPage;
  let fixture: ComponentFixture<CreateSelfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSelfPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSelfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
