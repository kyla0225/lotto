import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreateSelfPage } from './create-self.page';
import { TopHeaderComponent } from '../top-header/top-header.component';
import { BasicPopupPastComponent } from '../basic-popup-past/basic-popup-past.component';

const routes: Routes = [
  {
    path: '',
    component: CreateSelfPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CreateSelfPage,
    TopHeaderComponent,
    BasicPopupPastComponent
  ]
})
export class CreateSelfPageModule {}
