import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-self',
  templateUrl: './create-self.page.html',
  styleUrls: ['./create-self.page.scss'],
})
export class CreateSelfPage implements OnInit {

  lotto_arr = [];
  constructor() {
    for ( let j = 0; j < 45; j++ ) { this.lotto_arr[ j ] = ( j + 1 ) ; }
  }

  ngOnInit() {
  }

}
