import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'check-win-and', loadChildren: './check-win-and/check-win-and.module#CheckWinAndPageModule' },
  { path: 'self-typed-num', loadChildren: './self-typed-num/self-typed-num.module#SelfTypedNumPageModule' },
  { path: 'saved-number', loadChildren: './saved-number/saved-number.module#SavedNumberPageModule' },
  { path: 'create-self', loadChildren: './create-self/create-self.module#CreateSelfPageModule' },
  { path: 'create-random', loadChildren: './create-random/create-random.module#CreateRandomPageModule' },
  { path: 'fix-except-select', loadChildren: './fix-except-select/fix-except-select.module#FixExceptSelectPageModule' },
  { path: 'win-number-amount', loadChildren: './win-number-amount/win-number-amount.module#WinNumberAmountPageModule' },
  { path: 'except-fix-select', loadChildren: './except-fix-select/except-fix-select.module#ExceptFixSelectPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
