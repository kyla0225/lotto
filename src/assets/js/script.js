window.onload = function(){
    var mySwiper = new Swiper ('.swiper-first-to-fifth', {
        direction: 'horizontal',
        setWrapperSize: true,
        slidesPerView: 'auto'
        /* 넓이를 vw로 주면 먹긴하나 한 줄 정렬이 망가짐. 넓이 자동으로 단 scss상에서 min-width를 지정해 줄 것. 마진값은 굳이 여기서 안 줘도 먹긴하나 여기서 주는게 더 깔끔하긴 함. */
    })
}